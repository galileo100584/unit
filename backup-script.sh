#!/bin/bash

#Setup work-directory in container
cd root && mkdir work && cd work

#Updating package manager and installing some dev-tools
apt update 
apt -y install git

#Download and checkout the latest gauntlt version
git clone https://github.com/gauntlt/gauntlt
cd gauntlt

#Installing all dependencies for gauntlt-install script
apt -y install libcurl4-openssl-dev arachni curl python-setuptools python python-pip zlib1g-dev locate nmap

#Update rake (ruby package manager)
gem install rake

#Installing gauntlt with ruby package manager
gem install gauntlt

#install rvm
gem install rvm
