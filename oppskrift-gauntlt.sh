!#/bin/bash

cd root && mkdir work && cd work

apt update
apt -y install git && apt -y install vim

git clone https://github.com/gauntlt/gauntlt

apt -y install libcurl4-openssl-dev 
&& apt -y install arachni  
&& apt -y install curl 
&& apt -y install python-setuptools 
&& apt -y install python 
&& apt -y install zlib1g-dev 
&& apt -y install locate 
&& apt -y install nmap

gem install rake
sed -i 's/sudo //g' install_gauntlt_deps.sh
source ./install_gauntlt_deps.sh
gem install gauntlt
